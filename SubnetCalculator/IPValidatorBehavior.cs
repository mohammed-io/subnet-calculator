﻿using System;
using Xamarin.Forms;
using System.Text.RegularExpressions;

namespace SubnetCalculator
{
	public class IPValidatorBehavior : Behavior<Entry>
	{
		const string ipRegex = @"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}";

		static readonly BindablePropertyKey IsValidPropertyKey = BindableProperty.CreateReadOnly("IsValid", typeof(bool), typeof(IPValidatorBehavior), false);

		public static readonly BindableProperty IsValidProperty = IsValidPropertyKey.BindableProperty;

		public bool IsValid
		{
			get { return (bool)base.GetValue(IsValidProperty); }
			private set { base.SetValue(IsValidPropertyKey, value); }
		}

		protected override void OnAttachedTo(Entry bindable)
		{
			bindable.TextChanged += HandleTextChanged;
		}


		void HandleTextChanged(object sender, TextChangedEventArgs e)
		{
			IsValid = Regex.IsMatch(e.NewTextValue, ipRegex);
			((Entry)sender).TextColor = IsValid ? Color.Default : Color.Red;
		}

		protected override void OnDetachingFrom(Entry bindable)
		{
			bindable.TextChanged -= HandleTextChanged;
		}
	}
}
