﻿
using System;
using Xamarin.Forms;

namespace SubnetCalculator
{
	public class CidrValidatorBehavior : Behavior<Entry>
	{
		static readonly BindablePropertyKey IsValidPropertyKey = BindableProperty.CreateReadOnly("IsValid", typeof(bool), typeof(CidrValidatorBehavior), false);

		public static readonly BindableProperty IsValidProperty = IsValidPropertyKey.BindableProperty;

		public bool IsValid
		{
			get { return (bool)base.GetValue(IsValidProperty); }
			private set { base.SetValue(IsValidPropertyKey, value); }
		}

		protected override void OnAttachedTo(Entry bindable)
		{
			bindable.TextChanged += HandleTextChanged;
		}


		void HandleTextChanged(object sender, TextChangedEventArgs e)
		{
			var text = e.NewTextValue;
			var value = string.IsNullOrWhiteSpace(text) ? 0 : Convert.ToInt32(text);

			IsValid = value > 0 && value <= 31;

			((Entry)sender).TextColor = IsValid ? Color.Default : Color.Red;
		}

		protected override void OnDetachingFrom(Entry bindable)
		{
			bindable.TextChanged -= HandleTextChanged;
		}
	}
}
