﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace Horizon.Networker
{
	public class Networker
	{
		const string ipPattern = @"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}";
		const string cidrPattern = @"(\/)(\d{1,2})";

		public static readonly IP ClassAMask = new IP(new byte[] { 255, 0, 0, 0 });
		public static readonly IP ClassBMask = new IP(new byte[] { 255, 255, 0, 0 });
		public static readonly IP ClassCMask = new IP(new byte[] { 255, 255, 255, 0 });


		public IP Host { get; private set; }
		public IP Subnetmask { get; private set; }
		public IP Network { get; private set; }
		public IP First { get; private set; }
		public IP Last { get; private set; }
		public IP Broadcast { get; private set; }
		public long Total { get; private set; }
		public long Usable { get; private set; }

		public Networker(string ipAddress, int cidr = 0)
		{
			if (cidr > 30) throw new ArgumentException("Cannot use this cidr");

			Host = new IP(ipAddress);

			Initialize(cidr);
		}

		void Initialize(int cidr)
		{
			if (cidr == 0)
			{
				var firstOctet = Host.GetBytes()[0];

				if (firstOctet < 128)
				{
					Subnetmask = ClassAMask;
					Total = (long)Math.Pow(2, 24);
					Usable = Total - 2;
				}
				else if (firstOctet < 192)
				{
					Subnetmask = ClassBMask;
					Total = (long)Math.Pow(2, 16);
					Usable = Total - 2;
				}
				else if (firstOctet < 224)
				{
					Subnetmask = ClassCMask;
					Total = (long)Math.Pow(2, 8);
					Usable = Total - 2;
				}
				else
				{
					throw new Exception("The IP is not usable");
				}
			}
			// Else if CIDR is provided
			else
			{
				uint subnet = Convert.ToUInt32(new string('1', cidr) + new string('0', 32 - cidr), 2);

				Subnetmask = new IP(BitConverter.GetBytes(subnet).Reverse().ToArray());


				Total = (long)Math.Pow(2, 32 - cidr);
				Usable = Total - 2;
			}

			// Anding to get the network
			Network = Host & Subnetmask;

			// Getting the broadcast IP
			Broadcast = (Host | (Subnetmask ^ new IP(new byte[] { 255, 255, 255, 255 })));

			// The first host and the last host
			First = new IP(Network.GetInteger() + 1);
			Last = new IP(Broadcast.GetInteger() - 1);
		}

		public static Networker Parse(string fullIPAddress)
		{
			if (string.IsNullOrEmpty(fullIPAddress))
			{
				throw new ArgumentException("Is either null or empty");
			}

			if (!Regex.IsMatch(fullIPAddress, ipPattern + cidrPattern))
			{
				throw new ArgumentException("The IP is invalid");
			}

			int cidr = Convert.ToInt32(Regex.Match(fullIPAddress, cidrPattern).Captures[0].Value.Replace("/", ""));
			string ip = Regex.Match(fullIPAddress, ipPattern).Captures[0].Value;

			return new Networker(ip, cidr);
		}
	}
}
