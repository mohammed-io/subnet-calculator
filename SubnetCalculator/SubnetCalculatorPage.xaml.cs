﻿using System;
using Xamarin.Forms;
using System.Numerics;
using Horizon.Networker;

namespace SubnetCalculator
{
	public partial class SubnetCalculatorPage : ContentPage
	{
		void Handle_Clicked(object sender, EventArgs e)
		{
			try
			{
				if (CIDR.TextColor == Color.Red || IPEntry.TextColor == Color.Red)
				{
					throw new Exception("Please input a valid IP and CIDR");
				}
				else 
				{
					Networker networker = new Networker(IPEntry.Text, Convert.ToInt32(CIDR.Text));

					var vm = new ResultViewModel
					{
						NetworkIP = networker.Network.ToString(),
						FirstIP = networker.First.ToString(),
						LastIP = networker.Last.ToString(),
						Broadcast = networker.Broadcast.ToString(),
						Subnet = networker.Subnetmask.ToString(),
						Total = networker.Total.ToString(),
						Usable = networker.Usable.ToString()
					};

					Results.BindingContext = vm;

					Results.IsVisible = true;
				}
			}
			catch (Exception ex)
			{
				DisplayAlert("Error!", ex.Message, "OK");
			}
		}

		public SubnetCalculatorPage()
		{
			InitializeComponent();

			if (Device.OS == TargetPlatform.iOS)
			{
				Main.Margin = new Thickness(10, 20, 10, 10);
			}
		}
	}

	public class ResultViewModel
	{
		public string NetworkIP { get; set; }
		public string FirstIP { get; set; }
		public string LastIP { get; set; }
		public string Broadcast { get; set; }
		public string Subnet { get; set; }
		public string Total { get; set; }
		public string Usable { get; set; }
	}
}
