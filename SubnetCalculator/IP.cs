﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Numerics;

namespace Horizon.Networker
{
	public class IP
	{
		private string ipPattern = @"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}";

		//private uint ip;
		private byte[] ip;

		#region Constructors
		private IP()
		{

		}


		public IP(byte[] ipBytes)
		{
			if (ipBytes.Length != 4) throw new ArgumentException();

			ip = ipBytes;
		}

		public IP(string ip)
		{
			if (!Regex.IsMatch(ip, ipPattern)) throw new ArgumentException();

			this.ip = ip.Split('.')
				.Select(x => Convert.ToByte(x))
				.ToArray();

		}

		public IP(uint ip)
		{
			this.ip = BitConverter.GetBytes(ip).Reverse().ToArray();
		}

		#endregion Constructors

		public BigInteger GetBigInteger()
		{
			return BitConverter.ToUInt32(ip.Reverse().ToArray(), 0);
		}

		public uint GetInteger()
		{
			return BitConverter.ToUInt32(ip.Reverse().ToArray(), 0);
		}

		public byte[] GetBytes()
		{
			return ip;
		}

		public override string ToString()
		{

			return string.Join(".", ip.Select(x => x.ToString()));
		}

		#region Operators
		public static IP operator &(IP first, IP second)
		{
			var ip = new IP(first.GetInteger() & second.GetInteger());
			return ip;
		}

		public static IP operator ~(IP obj)
		{
			var ip = new IP(~obj.GetInteger());
			return ip;
		}

		public static IP operator ^(IP first, IP second)
		{
			var ip = new IP(first.GetInteger() ^ second.GetInteger());
			return ip;
		}

		public static IP operator |(IP first, IP second)
		{
			var ip = new IP(first.GetInteger() | second.GetInteger());
			return ip;
		}
		#endregion Operators
	}
}
